package com.raywenderlich.android.imet.ui.details

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.raywenderlich.android.imet.data.PeopleRepository
import com.raywenderlich.android.imet.data.model.People

class PeopleDetailsViewModel(private val peopleRepository: PeopleRepository) : ViewModel() {

    private val peopleId = MutableLiveData<Int>()

    fun getPeopleDetails(personId: Int): LiveData<People> {
        peopleId.value = personId
        val peopleDetails = Transformations.switchMap<Int, People>(peopleId) {
            peopleRepository.findPeople(it)
        }
        return peopleDetails
    }
}