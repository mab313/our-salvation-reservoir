package com.raywenderlich.android.imet.ui.add

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.raywenderlich.android.imet.data.PeopleRepository
import com.raywenderlich.android.imet.data.model.People

class AddPeopleViewModel(private val peopleRepository: PeopleRepository):ViewModel() {

    private val peopleDetails = MutableLiveData<People>()

    fun addPeople(people: People):LiveData<Boolean> {
        peopleDetails.value = people
        val detauils = Transformations.switchMap<People,Boolean>(peopleDetails){
            peopleRepository.insertPeople(it)
        }
        return detauils
    }

}