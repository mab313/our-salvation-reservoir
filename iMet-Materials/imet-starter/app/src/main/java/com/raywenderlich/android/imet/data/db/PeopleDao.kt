package com.raywenderlich.android.imet.data.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.raywenderlich.android.imet.data.model.People

@Dao
interface PeopleDao {

    @Query("Select * from People order by id DESC")
    fun getAll(): LiveData<List<People>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(people: People): Long

    @Query("Delete from People")
    fun deleteAll()

    @Query("Select * from People Where id = :id")
    fun find(id: Int): LiveData<People>

    @Query("Select * from People where name Like '%'|| :name ||'%'")
    fun findBy(name: String): LiveData<List<People>>
}