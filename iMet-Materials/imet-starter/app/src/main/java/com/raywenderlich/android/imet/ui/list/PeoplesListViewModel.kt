package com.raywenderlich.android.imet.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import com.raywenderlich.android.imet.data.PeopleRepository
import com.raywenderlich.android.imet.data.model.People

class PeoplesListViewModel(private val peopleRepository:PeopleRepository) : ViewModel() {

    private val peopleList = MediatorLiveData<List<People>>()

    init {
        getAllPeople()
    }

    fun getPeopleList():LiveData<List<People>> {
        return peopleList
    }

    fun getAllPeople() {
        peopleList.addSource(peopleRepository.getAllPeople()) { peoples ->
            peopleList.postValue(peoples)
        }
    }


    fun searchPeople(name: String) {
        peopleList.addSource(peopleRepository.findBy(name)){
            peopleList.postValue(it)
        }
    }

}