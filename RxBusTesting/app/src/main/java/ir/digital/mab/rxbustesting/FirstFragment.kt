package ir.digital.mab.rxbustesting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer


class FirstFragment : Fragment() {

    private var disposable:Disposable?=null

    private lateinit var txtName: TextView
    companion object{
        fun newInstance(): FirstFragment {
            val args = Bundle()

            val fragment = FirstFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_fragment_first, container, false)
    }

    override fun onViewCreated(
         view: View,
         savedInstanceState: Bundle?
    ) {

        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.next_fragment_btn).setOnClickListener {
            activity?.let {
                (it as MainActivity).startFragment(SecondFragment.newInstance())
            }
        }

        txtName = view.findViewById<TextView>(R.id.textViewName)

    }

    override fun onResume() {
        super.onResume()
        activity?.let {
             disposable = (it as MainActivity).getRxBus().listen(Consumer<Any> {
                if (it != null && it is String) {
                    txtName.text = it
                }
            })
            disposable?.run {  dispose()}
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.dispose()
    }

    private fun getInputObserver(): Observer<String> {
        return object : Observer<String> {
            override fun onComplete() {
                println("ONCOMPLETE")
            }

            override fun onSubscribe(d: Disposable) {
                println("onSubscribe")
                d.dispose()
            }

            override fun onNext(t: String) {
                txtName.text = t
                println("onNext" + t)
            }

            override fun onError(e: Throwable) {
                println(e.printStackTrace())
            }
        }
    }
}