package ir.digital.mab.rxbustesting

import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.BehaviorSubject


class RxBus private constructor(){

    companion object {
        private var mInstance: RxBus? = null
        fun getInstance(): RxBus {
            if (mInstance == null) {
                mInstance = RxBus()
            }
            return mInstance!!
        }
    }

    private val publisher = BehaviorSubject.create<Any>()
    fun publish(event: String) {
        publisher.onNext(event)
    }

    // Listen should return an Observable
    fun listen(action : Consumer<Any>): Disposable  {
        return publisher.subscribe(action)
    }

}