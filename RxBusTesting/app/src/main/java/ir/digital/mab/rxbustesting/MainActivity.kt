package ir.digital.mab.rxbustesting


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity

class MainActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startFragment(FirstFragment.newInstance())
    }

    fun startFragment(newInstance: Fragment) {
        supportFragmentManager.beginTransaction()
            .addToBackStack(newInstance::class.java.name)
            .replace(R.id.frame_layout,newInstance,newInstance::class.java.name)
            .commit()
    }

    fun getRxBus():RxBus{
        return RxBus.getInstance()
    }
}