package com.mabdigital.accordingly

import android.animation.Animator
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.mabdigital.accordingly.databinding.ItemFoodBinding
import com.mabdigital.accordingly.databinding.ItemFoodSwapableBinding
import java.util.ArrayList

class FoodListAdapter(private val dataList: ArrayList<FoodModel>) :
    RecyclerView.Adapter<FoodListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodListViewHolder {
        return FoodListViewHolder(
            ItemFoodBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: FoodListViewHolder, position: Int) {
        holder.bindData(getItem(position))
    }

    override fun getItemCount(): Int = dataList.size

    private fun getItem(position: Int): FoodModel = dataList[position]
}


class FoodListViewHolder(val binding: ItemFoodBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bindData(foodModel: FoodModel) {
        with(binding) {
            ivFood.setImageDrawable(
                ResourcesCompat.getDrawable(
                    itemView.context.resources,
                    foodModel.imgId,
                    null
                )
            )
            tvDescription.text = foodModel.description
            tvCalories.text = foodModel.calories
            tvRate.text = foodModel.rate
            tvTitle.text = foodModel.title
        }
    }
}

class FoodListAdapter2(private val dataList: ArrayList<FoodModel>) :
    RecyclerView.Adapter<FoodListViewHolder2>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodListViewHolder2 {
        return FoodListViewHolder2(
            ItemFoodSwapableBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: FoodListViewHolder2, position: Int) {
        holder.bindData(getItem(position))
    }

    override fun getItemCount(): Int = dataList.size

    private fun getItem(position: Int): FoodModel = dataList[position]
}

class FoodListViewHolder2(val binding: ItemFoodSwapableBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bindData(foodModel: FoodModel) {
        with(binding) {
            ivFood.setImageDrawable(
                ResourcesCompat.getDrawable(
                    itemView.context.resources,
                    foodModel.imgId,
                    null
                )
            )
            tvTitle.text = foodModel.title

            mainMotionLayout.addTransitionListener(object : MotionLayout.TransitionListener {
                override fun onTransitionStarted(p0: MotionLayout?, p1: Int, p2: Int) {}

                override fun onTransitionChange(p0: MotionLayout?, p1: Int, p2: Int, p3: Float) {}

                override fun onTransitionCompleted(p0: MotionLayout?, currentConstraintId : Int) {
                    when(currentConstraintId) {
                        R.id.end1 -> {
                            mainMotionLayout.animate()
                                .alpha(0f)
                                .setDuration(250)
                                .setListener(object : Animator.AnimatorListener {
                                    override fun onAnimationStart(p0: Animator?) {

                                    }

                                    override fun onAnimationEnd(p0: Animator?) {
                                        Toast.makeText(mainMotionLayout.context,"Dislike!",Toast.LENGTH_SHORT).show()
                                    }

                                    override fun onAnimationCancel(p0: Animator?) {

                                    }

                                    override fun onAnimationRepeat(p0: Animator?) {

                                    }
                                })
                        }
                        R.id.end2 -> {
                            mainMotionLayout.animate()
                                .alpha(0f)
                                .setDuration(250)
                                .setListener(object : Animator.AnimatorListener {
                                    override fun onAnimationStart(p0: Animator?) {

                                    }

                                    override fun onAnimationEnd(p0: Animator?) {
                                        Toast.makeText(mainMotionLayout.context,"like!",Toast.LENGTH_SHORT).show()
                                    }

                                    override fun onAnimationCancel(p0: Animator?) {

                                    }

                                    override fun onAnimationRepeat(p0: Animator?) {

                                    }
                                })
                        }
                    }
                }

                override fun onTransitionTrigger(
                    p0: MotionLayout?,
                    p1: Int,
                    p2: Boolean,
                    p3: Float
                ) {
                }
            })
        }
    }
}