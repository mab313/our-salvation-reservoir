package com.mabdigital.accordingly

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mabdigital.accordingly.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private val mainBinding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mainBinding.root)

        val adapter = FoodListAdapter2(foodDummyData)
        //GridLayoutManager(this,2, RecyclerView.VERTICAL,false)
        mainBinding.rvMain.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        mainBinding.rvMain.adapter = adapter
    }
}